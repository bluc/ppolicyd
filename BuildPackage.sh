#!/bin/bash
################################################################
#
# Build an installable package
#
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

VERSION='1.0'
# Get possible program options
while getopts hV: OPTION
do
    case ${OPTION} in
    V)  VERSION="$OPTARG"
        ;;
    *)  echo "Usage: $0 [-h] [-V version]"
        exit
        ;;
    esac
done
shift $((OPTIND - 1))

if [ "T$VERSION" = 'T1.0' ]
then
    while [ 1 ]
    do
        read -p 'Creating package version 1.0 - is this correct [y/n] ? ' YN
        [ "T${YN^^}" = 'TY' ] && break
        if [ "T${YN^^}" = 'TN' ]
        then
            echo "Rerun this script with the '-V' option"
            exit
        fi
    done  
fi

# At this time we can only create DEBIAN packages
if [ -f /etc/debian_version ]
then
    # Install GIT and FPM if necessary
    dpkg-query -W git &> /dev/null
    [ $? -ne 0 ] && apt-get -y install git
    dpkg-query -W ruby-dev &> /dev/null
    [ $? -ne 0 ] && apt-get -y install ruby-dev
    dpkg-query -W rubygems &> /dev/null
    [ $? -ne 0 ] && apt-get -y install rubygems
    [ -x /usr/local/bin/fpm ] || gem install fpm

    # Create the debian package
    trap "rm -rf /tmp/ppolicyd" EXIT
    mkdir -p /tmp/ppolicyd/tmp
    cp -r * /tmp/ppolicyd/tmp
    fpm -n ppolicyd -v $VERSION -a all -C /tmp/ppolicyd -m "consult@btoy1.net" \
      --after-install install.sh --description "Postfix Policy Daemon" \
      -f --package /tmp/ppolicyd_${VERSION}_all.deb -t deb \
      --depends libnet-server-perl --depends libnetaddr-ip-perl \
      --depends libnet-rblclient-perl --depends libmail-spf-perl \
      --depends libgeo-ipfree-perl --depends perl-doc \
      --url 'https://gitlab.com/bluc/ppolicyd' -s dir .
    RETCODE=$?
else
    echo "Unsupported Linux distribution"
    RETCODE=1
fi
exit $RETCODE
      