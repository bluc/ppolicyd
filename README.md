ppolicyd
========

Postfix Policy Daemon

Postfix can delegate policy decisions to an external server that runs
outside Postfix; this is called "SMTP Access Delegation".  The policy server
MUST reply with any action that is allowed in a Postfix SMTPD access table.

In  case  of trouble the policy server must not send a reply. Instead the
server must log a warning and disconnect.  Postfix will retry the request at
some later time.

ppolicyd performs a series of checks on an incoming email and reports back
to postfix what it thinks about it.  Postfix finally makes the decision
whether to reject or accept the email, or simply record a warning in its
logs.

The following checks are done by ppolicyd - for more details run:
ppolicyd --man

* Check for suspicious HELO name and an unresolved IP address
* Blacklist emails from certain countries or top-level domains
* Check for DNSBLs
* Check for SPF record for the client IP, sender, and HELO name
* Check whether email comes from an authorized IP address (if specified)

ppolicyd allows for exemptions for these checks.  All setups and
configuration is held in a central configuration file.

Zimbra integration
==================

In order to install and activate this program under Zimbra, use this script
(it assumes "Ubuntu" as the underlying Linux distribution):

``` Shell
#!/bin/bash
# Must be run as "root"
[ $EUID -ne 0 ] || exit 1

apt-get install unzip make
perl -e 'require Mail::SPF::Query'
[ $? -ne 0 ] && cpan -f install Mail::SPF::Query
[ -s /usr/local/src/ppolicyd.zip ] || wget https://gitlab.com/bluc/ppolicyd/-/archive/master/ppolicyd-master.zip -O /usr/local/src/ppolicyd.zip
cd /tmp
unzip /usr/local/src/ppolicyd.zip
cd ppolicyd-master
./install.sh
vim.tiny /etc/default/ppolicyd /etc/ppolicyd.conf

exit

# The following belongs into a cron job:

if [ -x /usr/sbin/ppolicyd ]
then
    # Enable "ppolicyd" for port 25
    nc -4zw 5 127.0.0.1 2522 &> /dev/null
    if [ $? -eq 0 ]
    then
        # ppolicyd is running
        SRR=$(postconf -h smtpd_recipient_restrictions)
        SRR=${SRR//, /,}
        SRR=${SRR/net,permit/net,check_policy_service inet:127.0.0.1:2522,permit}
        [ "$(postconf -h smtpd_ppolicyd_recipient_restrictions)" = "$SRR" ] || postconf -e "smtpd_ppolicyd_recipient_restrictions=$SRR"

        # Adapt "/opt/zimbra/postfix/conf/master.cf" and "/opt/zimbra/postfix/conf/master.cf.in"
        for F in master.cf master.cf.in
        do
            [ -z "$(grep 'smtpd_recipient_restrictions=$smtpd_ppolicyd_recipient_restrictions' /opt/zimbra/postfix/conf/$F)" ] || continue
            perl -p -e 's/^(smtp\s*inet.*$)/$1\n -o smtpd_recipient_restrictions=\$smtpd_ppolicyd_recipient_restrictions/' /opt/zimbra/postfix/conf/$F > /tmp/$F
            diff -wu /tmp/$F /opt/zimbra/postfix/conf/$F &> /dev/null
            if [ $? -ne 0 ]
            then
                cat /tmp/$F > /opt/zimbra/postfix/conf/$F
                RESTART_PF=2
            fi
        done
    fi
fi
[ $RESTART_PF -ne 0 ] && su - zimbra -c 'zmmtactl restart'
```

Copyright
=========

2014 Thomas Bullinger and B-LUC Consulting

License
=======

Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
