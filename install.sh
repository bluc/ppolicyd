#!/bin/bash
################################################################
#
# Install or update ppolicyd, the init script, and man page
# Install the conf file if not yet present
#
# (c) Copyright 2013 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
################################################################

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

if [ $EUID -ne 0 ]
then
	echo "You must be 'root' to execute this script"
	exit 1
fi

DEBIAN=0
[ -f /etc/debian_version ] && DEBIAN=1

if [ $DEBIAN -ne 0 ]
then
	DPKG2INSTALL=''
	# Install the necessary debian packages
	for DPKG in libnet-server-perl libnetaddr-ip-perl libnet-rblclient-perl libmail-spf-perl make libnet-dns-perl
	do
		# Skip already installed packages
		dpkg-query -W $DPKG 2> /dev/null
		[ $? -ne 0 ] && DPKG2INSTALL="$DPKG $DPKG2INSTALL"
	done
	# Install any missing packages
	[ -z "$DPKG2INSTALL" ] || apt-get install $DPKG2INSTALL
	apt-get install perl-doc

	# Horrible kludge since Debian uses an outdated version by default
	dpkg-query -W libgeo-ipfree-perl 2> /dev/null
	if [ $? -ne 0 ]
	then
		if [ -s /etc/lsb-release ]
		then
			# Ubuntu 
			apt-get install libgeo-ipfree-perl
		else
			# Vanilla debian
			apt-get -t testing install libgeo-ipfree-perl
		fi
	fi
        perl -e 'require Mail::SPF::Query'
        [ $? -ne 0 ] && cpan -f install Mail::SPF::Query

	# Make sure that the daemon script is invoked after a reboot
	[ -s /etc/init.d/ppolicyd ] && update-rc.d ppolicyd defaults
fi

# Define the path for the sources
if [ -s /tmp/ppolicyd-init ]
then
	# The debian package puts the sources there
	SRC_DIR='/tmp'
else
	# The standalone installation uses the sources in the current dir
	SRC_DIR='.'
fi

# Install the daemon script if necessary
INST_DAEMON=1
if [ -s /usr/sbin/ppolicyd ]
then
	diff ${SRC_DIR}/ppolicyd /usr/sbin/ppolicyd &> /dev/null
	INST_DAEMON=$?
fi
[ $INST_DAEMON -ne 0 ] && install -m 0755 -o root -g root ${SRC_DIR}/ppolicyd /usr/sbin/ppolicyd

# Install default configuration file if not yet there
INST_CONF=1
[ -s /usr/local/etc/ppolicyd.conf -o -s /etc/ppolicyd.conf ] && INST_CONF=0
[ $INST_CONF -ne 0 ] && install -m 0755 -o root -g root ${SRC_DIR}/ppolicyd.conf /etc/ppolicyd.conf

if [ -d /usr/share/doc ]
then
	# Install the documentation
	mkdir -p /usr/share/doc/ppolicyd
	/usr/sbin/ppolicyd -man > ${SRC_DIR}/ppolicyd.man
	install -m 644 ${SRC_DIR}/ppolicyd.conf ${SRC_DIR}/ppolicyd.man ${SRC_DIR}/ppolicyd-test.sh /usr/share/doc/ppolicyd
fi

# Download/update the GEO DB (otherwise ppolicyd won't start)
/usr/sbin/ppolicyd --getgeodb

if [ -d /etc/init.d ]
then
	# Install the init script
	if [ -s /etc/init.d/ppolicyd ]
	then
		diff ${SRC_DIR}/ppolicyd-init /etc/init.d/ppolicyd &> /dev/null
		INST_SCRIPT=$?
	else
		INST_SCRIPT=1
	fi
	if [ $INST_SCRIPT -ne 0 ]
	then
		install -m 0755 -o root -g root ${SRC_DIR}/ppolicyd-init /etc/init.d/ppolicyd
		update-rc.d ppolicyd defaults
	fi

	if [ -d /etc/default ]
	then
		if [ ! -s /etc/default/ppolicyd ]
		then
			cat << EOT > /etc/default/ppolicyd
# Set options for ppolicyd
DAEMON_ARGS='--max-children=32'
EOT
		fi
	fi

	# (Re-)Start the daemon script
	[ $INST_DAEMON -ne 0 -o $INST_SCRIPT -ne 0 ] && /etc/init.d/ppolicyd restart
else
	echo "Install 'ppolicyd-init' as an init script in your system"
fi

# We are done 
exit 0
