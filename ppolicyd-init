#! /bin/sh
# Version 20221108-133853 checked into repository
### BEGIN INIT INFO
# Provides:          ppolicyd
# Required-Start:    $local_fs $remote_fs $syslog $network
# Required-Stop:
# Should-Start:      
# Should-Stop:       
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Starts and stops the postfix policy daemon
# Description:       SMTP Access Delegation server
### END INIT INFO
################################################################
#
# $Id: ppolicyd-init,v 1.4 2011/03/04 12:46:17 root Exp $
#
################################################################

# PATH should only include /usr/* if it runs after the mountnfs.sh script
PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC='Postfix SMTP Access Delegation server'
NAME=ppolicyd
DAEMON=/usr/sbin/$NAME
DAEMON_ARGS=
PIDFILE=/tmp/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

# Load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.0-6) to ensure that this file is present.
. /lib/lsb/init-functions

#
# Function that starts the daemon/service
#
do_start()
{
#TB#	# Create/update the check script
#TB#	cat << EOT > /tmp/ppolicy-check.sh
#TB##!/bin/bash
#TB#PPD_PID=\$(ps h -o pid -C ppolicyd)
#TB#if [ -z "\$PPD_PID" ]
#TB#then
#TB#  # Start the ppolicyd script
#TB#  /etc/init.d/ppolicyd start
#TB#  exit
#TB#fi
#TB#PPD_SIZE=\$(ps h -o size -C ppolicyd)
#TB#if [ \$PPD_SIZE -gt $((256 * 1024)) ]
#TB#then
#TB#  # Reload the ppolicy script if it ate too much memory
#TB#  /etc/init.d/ppolicyd reload
#TB#fi
#TB#EOT
#TB#	chmod 0755 /tmp/ppolicy-check.sh
#TB#
#TB#	# Create/update the cronjob
#TB#	rm -f /etc/cron.d/ppolicyd
#TB#	cat << EOT > /etc/cron.d/ppolicyd
#TB## Make sure that we restart ppolicyd if it is down or occupies too much memory
#TB#* * * * *	root [ -x /tmp/ppolicy-check.sh ] && /tmp/ppolicy-check.sh
#TB#EOT

	# Return
	#   0 if daemon has been started
	#   1 if daemon was already running
	#   2 if daemon could not be started
	start-stop-daemon --start --quiet --pidfile $PIDFILE --exec $DAEMON --test > /dev/null \
		|| return 1
	start-stop-daemon --start --quiet --pidfile $PIDFILE --exec $DAEMON -- \
		$DAEMON_ARGS \
		|| return 2
	# Add code here, if necessary, that waits for the process to be ready
	# to handle requests from services started subsequently which depend
	# on this one.  As a last resort, sleep for some time.
}

#
# Function that stops the daemon/service
#
do_stop()
{
#TB#	# Remove the cronjob and the check script 
#TB#	rm -f /etc/cron.d/ppolicyd /tmp/ppolicy-check.sh
	if [ -s $PIDFILE ]
	then
		P_PID=`cat $PIDFILE`
	else
		P_PID=''
	fi

	# Return
	#   0 if daemon has been stopped
	#   1 if daemon was already stopped
	#   2 if daemon could not be stopped
	#   other if a failure occurred
	start-stop-daemon --stop --quiet --retry=TERM/30/KILL/5 --pidfile $PIDFILE --name $NAME
	RETVAL="$?"
	[ "$RETVAL" = 2 ] && return 2
	if [ ! -z "$P_PID" ]
	then
		# Wait for children to finish too if this is a daemon that forks
		# and if the daemon is only ever run from this initscript.
		kill $P_PID
		kill -0 $P_PID > /dev/null 2>&1
		while [ $? = 0 ]
		do
			sleep 0.5
			kill -0 $P_PID
		done
		RETVAL=0
	fi
	# Many daemons don't delete their pidfiles when they exit.
	rm -f $PIDFILE
	return "$RETVAL"
}

#
# Function that sends a SIGHUP to the daemon/service
#
do_reload() {
	#
	# If the daemon can reload its configuration without
	# restarting (for example, when it is sent a SIGHUP),
	# then implement that here.
	#
	start-stop-daemon --stop --signal 1 --quiet --pidfile $PIDFILE --name $NAME
	return 0
}

case "$1" in
  start)
	[ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC" "$NAME"
	do_start
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  stop)
	[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
	do_stop
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  status)
       status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
       ;;
  reload)
	#
	# If do_reload() is not implemented then leave this commented out
	# and leave 'force-reload' as an alias for 'restart'.
	#
	log_daemon_msg "Reloading $DESC" "$NAME"
	do_reload
	log_end_msg $?
	;;
  restart|force-reload)
	#
	# If the "reload" option is implemented then remove the
	# 'force-reload' alias
	#
	log_daemon_msg "Restarting $DESC" "$NAME"
	do_stop
	case "$?" in
	  0|1)
		do_start
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ;; # Old process is still running
			*) log_end_msg 1 ;; # Failed to start
		esac
		;;
	  *)
	  	# Failed to stop
		log_end_msg 1
		;;
	esac
	;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|status|reload|restart}" >&2
	exit 3
	;;
esac

:
